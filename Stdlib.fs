
module Stdlib

open Types
open PrettyPrint
open Eval
open Interop

let plus =
  let thing = either int (either float str)
  thing --> (thing --> thing)

let arith =
  let thing = either int float
  thing --> (thing --> thing)

let comparison =
  let arg = either int (either float str)
  arg --> (arg --> (either bool (either bool bool)))

let select
  (a : 'a -> 'a -> 'c)
  (b : 'b -> 'b -> 'd)
  (x : ('a, 'b) either)
  (y : ('a, 'b) either)
     : ('c, 'd) either
 =
  match x, y with
  | Left  x, Left  y -> Left  (a x y)
  | Right x, Right y -> Right (b x y)
  | _                -> raise (Problem "type mismatch")

let rec stdlib : frame =
  Map.ofList
    [
      // Ariphmetics.
      //
      "*", arith.back      (select (*) (*))
      "/", arith.back      (select (/) (/))
      "-", arith.back      (select (-) (-))
      "+", plus.back       (select (+) (select (+) (+)))
      "=", comparison.back (select (=) (select (=) (=)))

      ".", (object --> (str --> thunk)).back
        <| fun map i ->
          if Map.containsKey i map
          then map.[i]
          else raise <| Problem (sprintf "Not there %s" i)

      "extend", (object --> (object --> object)).back
        <| fun a b ->
          Map.fold (fun m k v -> Map.add k v m) a b

      // Debug print wrapper. Returns its second argument.
      //
      "trace", (str --> (thunk --> thunk)).back
        <| fun mark v ->
          printf "%s " mark
          pp 0 v
          v

      "test", (list (pair (str, int))).back
        [ "foo", 1
          "bar", 2
        ]

      "seq", (value --> (thunk --> thunk)).back
        <| fun x y -> y

      // Generate an error. Use when the impossible has happened,
      // and the program must be terminated.
      //
      "error", (str --> value).back
        <| fun msg -> VError msg
    ]
