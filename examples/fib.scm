(let
  ((sqr (-> x (* x x)))
   (x2  (-> x (+ x x)))
   (obj (new sqr x2))
   (fib (-> x
      (? (= x 0) True () 1
      (? (= x 1) True () 1
      (? (= x 11) True () ('foo' 1)
        (+ (fib (- x 1)) (fib (- x 2))))))
   ))
   (flip (-> f (-> x (-> y
      (f y x)
   ))))
  )
  (sqr (trace 'fib 12 =' (fib 12)))
)