(let
  ( (foo (-> x
      (let
        ( (sqr (* x x))
        )

        (bar sqr))
      ))
    (bar (-> x (qux (- x 1))))

    (qux (-> x
      (let
        ( (sqr (* x 'kek'))
        )
        (sqr x))))
  )

  (foo 1)
)