
module Types

[<StructuredFormatDisplay("{raw}")>]
type name =
  { raw   : string
    place : place
  }

and place =
  { line   : int
    col    : int
    source : string
  }

// When you need a name and all you have is string.
//
let n (raw : string) : name =
  { raw   = raw
    place =
      { line   = 1
        col    = 1
        source = "-"
      }
  }

let raw (name : name) : string = name.raw

type constant =
  | Integer of bigint
  | String  of string
  | Float   of double

// Abstract syntax tree for the language we will evaluate.
//
type program =
  | Var     of name
  | Let     of (name, program) Map * program
  | Call    of program * program
  | IfMatch of program * name * name list * program * program
  | Create  of name list
  | Ctor    of name * name list
  | Lambda  of name * program
  | Const   of constant
  | Invoke  of name * program

// Stack is a list of stack frames.
//
// That's a part of runtime; I have no clue, how to move it out here.
//
type stack =
  stack_frame list

and frame =
  (string, thunk) Map

// A stack frame. Also used as object/variant body.
//
and stack_frame =
  | LetFrame of frame
  | ArgFrame of string * thunk
  | Invoked  of name

and thunk =
  value Lazy

// A runtime representation for a value (= the result of computation).
//
and value =
  | VConst   of constant                      // constant
  | VObject  of frame                         // object
  | VClosure of stack * string * program      // a function
  | VBIF     of (thunk -> thunk)              // a builtin function
  | VVariant of string * frame * string list  // a "variant"
  | VError   of string                        // a black hole
